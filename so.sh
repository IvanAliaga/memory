#/bin/bash

MEM_TOTAL="$(free -m | awk 'NR==2{printf '\"%i\"', $2 }')"
MEM_USED="$(free -m | awk 'NR==2{printf '\"%i\"', $3 }')"
MEM_FREE="$(free -m | awk 'NR==2{printf '\"%i\"', $4 }')"
MEM_SHARED="$(free -m | awk 'NR==2{printf '\"%i\"', $5 }')"
MEM_BC="$(free -m | awk 'NR==2{printf '\"%i\"', $6 }')"
MEM_AVAILABLE="$(free -m | awk 'NR==2{printf '\"%i\"', $7 }')"
echo "{\"mem_total\":${MEM_TOTAL}, 
	   \"mem_used\":${MEM_USED},
       \"mem_free\":${MEM_FREE},
	   \"mem_shared\":${MEM_SHARED},
	   \"mem_bc\":${MEM_BC},
	   \"mem_available\":${MEM_AVAILABLE}}"
